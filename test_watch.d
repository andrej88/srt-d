#!/usr/bin/env dub
/+ dub.sdl:
    name "test-watch"
    dependency "fswatch" version="~>0.5"
    license "public domain"
+/

import core.thread : Thread;
import fswatch : FileChangeEvent, FileWatch;
import std.datetime;
import std.process;

void main(string[] args)
{
    FileWatch watcher = FileWatch("source/", true);

    runTests(args);

    while (true)
    {
        if (watcher.getEvents().length > 0) runTests(args);

        Thread.sleep(500.msecs);
    }
}

private void runTests(string[] args)
{
    Pid clearPid = spawnShell("clear");
    wait(clearPid);
    Pid dubTestPid = spawnProcess(["dub", "test", "--compiler=dmd", "--"] ~ args);
    wait(dubTestPid);
}
