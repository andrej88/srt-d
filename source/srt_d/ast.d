module srt_d.ast;

import pegged.peg : ParseTree;
import std.conv : to;


package struct Line
{
    string text;
}

package string buildLine(ParseTree pt)
in(pt.name == "SrtGrammar.Line", "buildLine requires a ParseTree of type 'SrtGrammar.Line', but received '" ~ pt.name ~ "'.")
{
    return pt.matches[0];
}

package string[2] buildText(ParseTree pt)
in(pt.name == "SrtGrammar.Text", "buildText requires a ParseTree of type 'SrtGrammar.Text', but received '" ~ pt.name ~ "'.")
{
    return [
        buildLine(pt.children[0]),
        buildLine(pt.children[1])
    ];
}

package int buildHours(ParseTree pt)
{
    return pt.matches[0].to!int;
}
