module srt_d.parse_test;

import core.time;
import pegged.tester.grammartester;
import srt_d.parse;


private alias Tester(string rule) = GrammarTester!(SrtGrammar, rule);

@("Parse Time")
unittest
{
    Tester!"Time" tester = new Tester!"Time";
    tester.assertSimilar("00:02:17,440", `
        Time -> {
            Hours
            Minutes
            Seconds
            Milliseconds
        }
    `);
}

@("Parse Duration")
unittest
{
    Tester!"Duration" tester = new Tester!"Duration";
    tester.assertSimilar("00:02:17,440 --> 00:02:20,375", `
        Duration -> {
            Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
            Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
        }
    `);
}

@("Parse Counter")
unittest
{
    Tester!"Counter" tester = new Tester!"Counter";
    tester.assertSimilar("5", `Counter`);
}

@("Parse One-Line Text")
unittest
{
    Tester!"Text" tester = new Tester!"Text";
    tester.assertSimilar("Play it, Sam.", `
        Text -> Line
    `);
}

@("Parse Two-Line Text")
unittest
{
    Tester!"Text" tester = new Tester!"Text";
    tester.assertSimilar("Louis, I think this is the\r\nbeginning of a beautiful friendship.",
    `
        Text -> {
            Line
            Line
        }
    `);
}

@("Parse Subtitle")
unittest
{
    Tester!"Subtitle" tester = new Tester!"Subtitle";
    tester.assertSimilar(
        "1\r\n" ~
        "00:02:17,440 --> 00:02:20,375\r\n" ~
        "Of all the gin joints in all the towns\r\n" ~
        "in all the world, she walks into mine.\r\n",
        `
        Subtitle -> {
            Counter
            Duration -> {
                Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
                Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
            }
            Text -> ^Line -> ^Line
        }
        `
    );
}

@("Parse Subtitles")
unittest
{
    Tester!"Subtitles" tester = new Tester!"Subtitles";
    tester.assertSimilar(
        "1\r\n" ~
        "00:00:00,000 --> 00:00:03,352\r\n" ~
        "I have often speculated on why\r\n" ~
        "you do not return to America.\r\n" ~
        "\r\n" ~
        "2\r\n" ~
        "00:00:03,352 --> 00:00:07,020\r\n" ~
        "Did you abscond with the church\r\n" ~
        "funds? Did you run off with a\r\n" ~
        "\r\n" ~
        "3\r\n" ~
        "00:00:07,020 --> 00:00:09,857\r\n" ~
        "senator's wife? I should like to\r\n" ~
        "think you killed a man.\r\n" ~
        "\r\n" ~
        "4\r\n" ~
        "00:00:09,857 --> 00:00:11,112\r\n" ~
        "It is the romantic in me.\r\n" ~
        "\r\n" ~
        "5\r\n" ~
        "00:00:11,401 --> 00:00:12,940\r\n" ~
        "It was a combination of all three.\r\n",
        `
        Subtitles -> {
            Subtitle -> {
                Counter
                Duration -> {
                    Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
                    Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
                }
                Text -> ^Line -> ^Line
            }
            Subtitle -> {
                Counter
                Duration -> {
                    Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
                    Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
                }
                Text -> ^Line -> ^Line
            }
            Subtitle -> {
                Counter
                Duration -> {
                    Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
                    Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
                }
                Text -> ^Line -> ^Line
            }
            Subtitle -> {
                Counter
                Duration -> {
                    Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
                    Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
                }
                Text -> ^Line
            }
            Subtitle -> {
                Counter
                Duration -> {
                    Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
                    Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
                }
                Text -> ^Line
            }
        }
        `
    );
}

@("Parse Subtitles, no trailing newline")
unittest
{
    Tester!"Subtitles" tester = new Tester!"Subtitles";
    tester.assertSimilar(
        "1\r\n" ~
        "00:00:00,000 --> 00:00:03,352\r\n" ~
        "Here's looking at you, kid.",
        `
        Subtitles -> {
            Subtitle -> {
                Counter
                Duration -> {
                    Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
                    Time -> ^Hours -> ^Minutes -> ^Seconds -> ^Milliseconds
                }
                Text -> Line
            }
        }
        `
    );
}
