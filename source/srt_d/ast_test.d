module srt_d.ast_test;

import exceeds_expectations;
import pegged.peg;
import srt_d.ast;
import srt_d.parse;

// NOTE: ParseTrees are not automatically decimated when
//       parsing a rule directly (SrtGrammar.Line(), etc.),
//       which is why it's called manually.

@("Line – ParseTree to string")
unittest
{
    string testString = "A wizard is never late, Frodo Baggins.";
    ParseTree input = SrtGrammar.decimateTree(SrtGrammar.Line(testString));
    expect(buildLine(input)).toEqual(testString);
}

@("Text – ParseTree to string[2]")
unittest
{
    string testString = "Nor is he early.\r\nHe arrives precisely when he means to.";
    ParseTree input = SrtGrammar.decimateTree(SrtGrammar.Text(testString));
    expect(buildText(input)).toEqual([
        "Nor is he early.",
        "He arrives precisely when he means to."
    ]);
}

@("Hours – ParseTree to int")
unittest
{
    foreach (string testString, int result; [
        "00": 0, "09": 9, "21": 21
    ])
    {
        ParseTree input = SrtGrammar.decimateTree(SrtGrammar.Hours(testString));
        expect(buildHours(input)).toEqual(result);
    }
}

@("Minutes – ParseTree to int")
unittest
{
    foreach (string testString, int result; [
        "00": 0, "09": 9, "21": 21
    ])
    {
        ParseTree input = SrtGrammar.decimateTree(SrtGrammar.Hours(testString));
        expect(buildHours(input)).toEqual(result);
    }
}
