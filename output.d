struct GenericSrtGrammar(TParseTree)
{
    import std.functional : toDelegate;
    import pegged.dynamic.grammar;
    static import pegged.peg;

    struct SrtGrammar
    {
        enum name = "SrtGrammar";
        static ParseTree delegate(ParseTree)[string] before;
        static ParseTree delegate(ParseTree)[string] after;
        static ParseTree delegate(ParseTree)[string] rules;
        import std.typecons : Tuple, tuple;

        static TParseTree[Tuple!(string, size_t)] memo;
        static this()
        {
            rules["Subtitles"] = toDelegate(&Subtitles);
            rules["Subtitle"] = toDelegate(&Subtitle);
            rules["Counter"] = toDelegate(&Counter);
            rules["Duration"] = toDelegate(&Duration);
            rules["Time"] = toDelegate(&Time);
            rules["Hours"] = toDelegate(&Hours);
            rules["Minutes"] = toDelegate(&Minutes);
            rules["Seconds"] = toDelegate(&Seconds);
            rules["Milliseconds"] = toDelegate(&Milliseconds);
            rules["Text"] = toDelegate(&Text);
            rules["Line"] = toDelegate(&Line);
            rules["Spacing"] = toDelegate(&Spacing);
        }

        template hooked(alias r, string name)
        {
            static ParseTree hooked(ParseTree p)
            {
                ParseTree result;

                if (name in before)
                {
                    result = before[name](p);
                    if (result.successful)
                        return result;
                }

                result = r(p);
                if (result.successful || name !in after)
                    return result;

                result = after[name](p);
                return result;
            }

            static ParseTree hooked(string input)
            {
                return hooked!(r, name)(ParseTree("", false, [], input));
            }
        }

        static void addRuleBefore(string parentRule, string ruleSyntax)
        {
            // enum name is the current grammar name
            DynamicGrammar dg = pegged.dynamic.grammar.grammar(name ~ ": " ~ ruleSyntax, rules);
            foreach (ruleName, rule; dg.rules)
                if (ruleName != "Spacing") // Keep the local Spacing rule, do not overwrite it
                    rules[ruleName] = rule;
            before[parentRule] = rules[dg.startingRule];
        }

        static void addRuleAfter(string parentRule, string ruleSyntax)
        {
            // enum name is the current grammar named
            DynamicGrammar dg = pegged.dynamic.grammar.grammar(name ~ ": " ~ ruleSyntax, rules);
            foreach (name, rule; dg.rules)
            {
                if (name != "Spacing")
                    rules[name] = rule;
            }
            after[parentRule] = rules[dg.startingRule];
        }

        static bool isRule(string s)
        {
            import std.algorithm : startsWith;

            return s.startsWith("SrtGrammar.");
        }

        mixin decimateTree;

        alias spacing Spacing;

        static TParseTree Subtitles(TParseTree p)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.and!(Subtitle,
                        pegged.peg.zeroOrMore!(pegged.peg.and!(pegged.peg.discard!(
                        pegged.peg.literal!("\r\n\r\n")), Subtitle)),
                        pegged.peg.discard!(pegged.peg.zeroOrMore!(pegged.peg.literal!("\r\n"))),
                        endOfInput), "SrtGrammar.Subtitles")(p);
            }
            else
            {
                if (auto m = tuple(`Subtitles`, p.end) in memo)
                    return *m;
                else
                {
                    TParseTree result = hooked!(pegged.peg.defined!(pegged.peg.and!(Subtitle,
                            pegged.peg.zeroOrMore!(pegged.peg.and!(pegged.peg.discard!(
                            pegged.peg.literal!("\r\n\r\n")), Subtitle)),
                            pegged.peg.discard!(pegged.peg.zeroOrMore!(pegged.peg.literal!("\r\n"))),
                            endOfInput), "SrtGrammar.Subtitles"), "Subtitles")(p);
                    memo[tuple(`Subtitles`, p.end)] = result;
                    return result;
                }
            }
        }

        static TParseTree Subtitles(string s)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.and!(Subtitle,
                        pegged.peg.zeroOrMore!(pegged.peg.and!(pegged.peg.discard!(
                        pegged.peg.literal!("\r\n\r\n")), Subtitle)),
                        pegged.peg.discard!(pegged.peg.zeroOrMore!(pegged.peg.literal!("\r\n"))),
                        endOfInput), "SrtGrammar.Subtitles")(TParseTree("", false, [
                        ], s));
            }
            else
            {
                forgetMemo();
                return hooked!(pegged.peg.defined!(pegged.peg.and!(Subtitle,
                        pegged.peg.zeroOrMore!(pegged.peg.and!(pegged.peg.discard!(
                        pegged.peg.literal!("\r\n\r\n")), Subtitle)),
                        pegged.peg.discard!(pegged.peg.zeroOrMore!(pegged.peg.literal!("\r\n"))),
                        endOfInput), "SrtGrammar.Subtitles"), "Subtitles")(TParseTree("",
                        false, [], s));
            }
        }

        static string Subtitles(GetName g)
        {
            return "SrtGrammar.Subtitles";
        }

        static TParseTree Subtitle(TParseTree p)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.and!(Counter,
                        pegged.peg.discard!(pegged.peg.literal!("\r\n")),
                        Duration, pegged.peg.discard!(pegged.peg.literal!("\r\n")), Text),
                        "SrtGrammar.Subtitle")(p);
            }
            else
            {
                if (auto m = tuple(`Subtitle`, p.end) in memo)
                    return *m;
                else
                {
                    TParseTree result = hooked!(pegged.peg.defined!(pegged.peg.and!(Counter,
                            pegged.peg.discard!(pegged.peg.literal!("\r\n")),
                            Duration, pegged.peg.discard!(pegged.peg.literal!("\r\n")), Text),
                            "SrtGrammar.Subtitle"), "Subtitle")(p);
                    memo[tuple(`Subtitle`, p.end)] = result;
                    return result;
                }
            }
        }

        static TParseTree Subtitle(string s)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.and!(Counter,
                        pegged.peg.discard!(pegged.peg.literal!("\r\n")),
                        Duration, pegged.peg.discard!(pegged.peg.literal!("\r\n")), Text),
                        "SrtGrammar.Subtitle")(TParseTree("", false, [], s));
            }
            else
            {
                forgetMemo();
                return hooked!(pegged.peg.defined!(pegged.peg.and!(Counter,
                        pegged.peg.discard!(pegged.peg.literal!("\r\n")),
                        Duration, pegged.peg.discard!(pegged.peg.literal!("\r\n")), Text),
                        "SrtGrammar.Subtitle"), "Subtitle")(TParseTree("", false, [
                        ], s));
            }
        }

        static string Subtitle(GetName g)
        {
            return "SrtGrammar.Subtitle";
        }

        static TParseTree Counter(TParseTree p)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(digits, "SrtGrammar.Counter")(p);
            }
            else
            {
                if (auto m = tuple(`Counter`, p.end) in memo)
                    return *m;
                else
                {
                    TParseTree result = hooked!(pegged.peg.defined!(digits,
                            "SrtGrammar.Counter"), "Counter")(p);
                    memo[tuple(`Counter`, p.end)] = result;
                    return result;
                }
            }
        }

        static TParseTree Counter(string s)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(digits,
                        "SrtGrammar.Counter")(TParseTree("", false, [], s));
            }
            else
            {
                forgetMemo();
                return hooked!(pegged.peg.defined!(digits, "SrtGrammar.Counter"), "Counter")(TParseTree("",
                        false, [], s));
            }
        }

        static string Counter(GetName g)
        {
            return "SrtGrammar.Counter";
        }

        static TParseTree Duration(TParseTree p)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.and!(Time,
                        pegged.peg.discard!(pegged.peg.literal!(" --> ")), Time),
                        "SrtGrammar.Duration")(p);
            }
            else
            {
                if (auto m = tuple(`Duration`, p.end) in memo)
                    return *m;
                else
                {
                    TParseTree result = hooked!(pegged.peg.defined!(pegged.peg.and!(Time,
                            pegged.peg.discard!(pegged.peg.literal!(" --> ")), Time),
                            "SrtGrammar.Duration"), "Duration")(p);
                    memo[tuple(`Duration`, p.end)] = result;
                    return result;
                }
            }
        }

        static TParseTree Duration(string s)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.and!(Time,
                        pegged.peg.discard!(pegged.peg.literal!(" --> ")), Time),
                        "SrtGrammar.Duration")(TParseTree("", false, [], s));
            }
            else
            {
                forgetMemo();
                return hooked!(pegged.peg.defined!(pegged.peg.and!(Time,
                        pegged.peg.discard!(pegged.peg.literal!(" --> ")), Time),
                        "SrtGrammar.Duration"), "Duration")(TParseTree("", false, [
                        ], s));
            }
        }

        static string Duration(GetName g)
        {
            return "SrtGrammar.Duration";
        }

        static TParseTree Time(TParseTree p)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.and!(Hours,
                        pegged.peg.discard!(pegged.peg.literal!(":")),
                        Minutes, pegged.peg.discard!(pegged.peg.literal!(":")),
                        Seconds, pegged.peg.discard!(pegged.peg.literal!(",")), Milliseconds),
                        "SrtGrammar.Time")(p);
            }
            else
            {
                if (auto m = tuple(`Time`, p.end) in memo)
                    return *m;
                else
                {
                    TParseTree result = hooked!(pegged.peg.defined!(pegged.peg.and!(Hours,
                            pegged.peg.discard!(pegged.peg.literal!(":")),
                            Minutes, pegged.peg.discard!(pegged.peg.literal!(":")),
                            Seconds, pegged.peg.discard!(pegged.peg.literal!(",")),
                            Milliseconds), "SrtGrammar.Time"), "Time")(p);
                    memo[tuple(`Time`, p.end)] = result;
                    return result;
                }
            }
        }

        static TParseTree Time(string s)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.and!(Hours,
                        pegged.peg.discard!(pegged.peg.literal!(":")),
                        Minutes, pegged.peg.discard!(pegged.peg.literal!(":")),
                        Seconds, pegged.peg.discard!(pegged.peg.literal!(",")), Milliseconds),
                        "SrtGrammar.Time")(TParseTree("", false, [], s));
            }
            else
            {
                forgetMemo();
                return hooked!(pegged.peg.defined!(pegged.peg.and!(Hours,
                        pegged.peg.discard!(pegged.peg.literal!(":")),
                        Minutes, pegged.peg.discard!(pegged.peg.literal!(":")),
                        Seconds, pegged.peg.discard!(pegged.peg.literal!(",")), Milliseconds),
                        "SrtGrammar.Time"), "Time")(TParseTree("", false, [], s));
            }
        }

        static string Time(GetName g)
        {
            return "SrtGrammar.Time";
        }

        static TParseTree Hours(TParseTree p)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.fuse!(pegged.peg.and!(pegged.peg.charRange!('0',
                        '9'), pegged.peg.charRange!('0', '9'))), "SrtGrammar.Hours")(p);
            }
            else
            {
                if (auto m = tuple(`Hours`, p.end) in memo)
                    return *m;
                else
                {
                    TParseTree result = hooked!(pegged.peg.defined!(
                            pegged.peg.fuse!(pegged.peg.and!(pegged.peg.charRange!('0',
                            '9'), pegged.peg.charRange!('0', '9'))), "SrtGrammar.Hours"), "Hours")(
                            p);
                    memo[tuple(`Hours`, p.end)] = result;
                    return result;
                }
            }
        }

        static TParseTree Hours(string s)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.fuse!(pegged.peg.and!(pegged.peg.charRange!('0',
                        '9'), pegged.peg.charRange!('0', '9'))), "SrtGrammar.Hours")(TParseTree("",
                        false, [], s));
            }
            else
            {
                forgetMemo();
                return hooked!(pegged.peg.defined!(pegged.peg.fuse!(
                        pegged.peg.and!(pegged.peg.charRange!('0', '9'),
                        pegged.peg.charRange!('0', '9'))), "SrtGrammar.Hours"), "Hours")(TParseTree("",
                        false, [], s));
            }
        }

        static string Hours(GetName g)
        {
            return "SrtGrammar.Hours";
        }

        static TParseTree Minutes(TParseTree p)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.fuse!(pegged.peg.and!(pegged.peg.charRange!('0',
                        '9'), pegged.peg.charRange!('0', '9'))), "SrtGrammar.Minutes")(p);
            }
            else
            {
                if (auto m = tuple(`Minutes`, p.end) in memo)
                    return *m;
                else
                {
                    TParseTree result = hooked!(pegged.peg.defined!(
                            pegged.peg.fuse!(pegged.peg.and!(pegged.peg.charRange!('0',
                            '9'), pegged.peg.charRange!('0', '9'))), "SrtGrammar.Minutes"),
                            "Minutes")(p);
                    memo[tuple(`Minutes`, p.end)] = result;
                    return result;
                }
            }
        }

        static TParseTree Minutes(string s)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.fuse!(pegged.peg.and!(pegged.peg.charRange!('0',
                        '9'), pegged.peg.charRange!('0', '9'))), "SrtGrammar.Minutes")(TParseTree("",
                        false, [], s));
            }
            else
            {
                forgetMemo();
                return hooked!(pegged.peg.defined!(pegged.peg.fuse!(
                        pegged.peg.and!(pegged.peg.charRange!('0', '9'),
                        pegged.peg.charRange!('0', '9'))), "SrtGrammar.Minutes"), "Minutes")(TParseTree("",
                        false, [], s));
            }
        }

        static string Minutes(GetName g)
        {
            return "SrtGrammar.Minutes";
        }

        static TParseTree Seconds(TParseTree p)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.fuse!(pegged.peg.and!(pegged.peg.charRange!('0',
                        '9'), pegged.peg.charRange!('0', '9'))), "SrtGrammar.Seconds")(p);
            }
            else
            {
                if (auto m = tuple(`Seconds`, p.end) in memo)
                    return *m;
                else
                {
                    TParseTree result = hooked!(pegged.peg.defined!(
                            pegged.peg.fuse!(pegged.peg.and!(pegged.peg.charRange!('0',
                            '9'), pegged.peg.charRange!('0', '9'))), "SrtGrammar.Seconds"),
                            "Seconds")(p);
                    memo[tuple(`Seconds`, p.end)] = result;
                    return result;
                }
            }
        }

        static TParseTree Seconds(string s)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.fuse!(pegged.peg.and!(pegged.peg.charRange!('0',
                        '9'), pegged.peg.charRange!('0', '9'))), "SrtGrammar.Seconds")(TParseTree("",
                        false, [], s));
            }
            else
            {
                forgetMemo();
                return hooked!(pegged.peg.defined!(pegged.peg.fuse!(
                        pegged.peg.and!(pegged.peg.charRange!('0', '9'),
                        pegged.peg.charRange!('0', '9'))), "SrtGrammar.Seconds"), "Seconds")(TParseTree("",
                        false, [], s));
            }
        }

        static string Seconds(GetName g)
        {
            return "SrtGrammar.Seconds";
        }

        static TParseTree Milliseconds(TParseTree p)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.fuse!(pegged.peg.and!(pegged.peg.charRange!('0',
                        '9'), pegged.peg.charRange!('0', '9'), pegged.peg.charRange!('0', '9'))),
                        "SrtGrammar.Milliseconds")(p);
            }
            else
            {
                if (auto m = tuple(`Milliseconds`, p.end) in memo)
                    return *m;
                else
                {
                    TParseTree result = hooked!(pegged.peg.defined!(
                            pegged.peg.fuse!(pegged.peg.and!(pegged.peg.charRange!('0',
                            '9'), pegged.peg.charRange!('0', '9'),
                            pegged.peg.charRange!('0', '9'))), "SrtGrammar.Milliseconds"),
                            "Milliseconds")(p);
                    memo[tuple(`Milliseconds`, p.end)] = result;
                    return result;
                }
            }
        }

        static TParseTree Milliseconds(string s)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.fuse!(pegged.peg.and!(pegged.peg.charRange!('0',
                        '9'), pegged.peg.charRange!('0', '9'), pegged.peg.charRange!('0', '9'))),
                        "SrtGrammar.Milliseconds")(TParseTree("", false, [], s));
            }
            else
            {
                forgetMemo();
                return hooked!(pegged.peg.defined!(pegged.peg.fuse!(
                        pegged.peg.and!(pegged.peg.charRange!('0', '9'),
                        pegged.peg.charRange!('0', '9'), pegged.peg.charRange!('0', '9'))),
                        "SrtGrammar.Milliseconds"), "Milliseconds")(TParseTree("", false, [
                        ], s));
            }
        }

        static string Milliseconds(GetName g)
        {
            return "SrtGrammar.Milliseconds";
        }

        static TParseTree Text(TParseTree p)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.and!(Line,
                        pegged.peg.option!(pegged.peg.and!(pegged.peg.discard!(pegged.peg.literal!("\r\n")),
                        Line))), "SrtGrammar.Text")(p);
            }
            else
            {
                if (auto m = tuple(`Text`, p.end) in memo)
                    return *m;
                else
                {
                    TParseTree result = hooked!(pegged.peg.defined!(pegged.peg.and!(Line,
                            pegged.peg.option!(pegged.peg.and!(pegged.peg.discard!(pegged.peg.literal!("\r\n")),
                            Line))), "SrtGrammar.Text"), "Text")(p);
                    memo[tuple(`Text`, p.end)] = result;
                    return result;
                }
            }
        }

        static TParseTree Text(string s)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.and!(Line,
                        pegged.peg.option!(pegged.peg.and!(pegged.peg.discard!(pegged.peg.literal!("\r\n")),
                        Line))), "SrtGrammar.Text")(TParseTree("", false, [], s));
            }
            else
            {
                forgetMemo();
                return hooked!(pegged.peg.defined!(pegged.peg.and!(Line,
                        pegged.peg.option!(pegged.peg.and!(pegged.peg.discard!(pegged.peg.literal!("\r\n")),
                        Line))), "SrtGrammar.Text"), "Text")(TParseTree("", false, [
                        ], s));
            }
        }

        static string Text(GetName g)
        {
            return "SrtGrammar.Text";
        }

        static TParseTree Line(TParseTree p)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.fuse!(
                        pegged.peg.oneOrMore!(pegged.peg.and!(
                        pegged.peg.negLookahead!(pegged.peg.or!(pegged.peg.literal!("\r"),
                        pegged.peg.literal!("\n"))), pegged.peg.any))), "SrtGrammar.Line")(p);
            }
            else
            {
                if (auto m = tuple(`Line`, p.end) in memo)
                    return *m;
                else
                {
                    TParseTree result = hooked!(pegged.peg.defined!(pegged.peg.fuse!(
                            pegged.peg.oneOrMore!(pegged.peg.and!(
                            pegged.peg.negLookahead!(pegged.peg.or!(pegged.peg.literal!("\r"),
                            pegged.peg.literal!("\n"))), pegged.peg.any))), "SrtGrammar.Line"),
                            "Line")(p);
                    memo[tuple(`Line`, p.end)] = result;
                    return result;
                }
            }
        }

        static TParseTree Line(string s)
        {
            if (__ctfe)
            {
                return pegged.peg.defined!(pegged.peg.fuse!(
                        pegged.peg.oneOrMore!(pegged.peg.and!(
                        pegged.peg.negLookahead!(pegged.peg.or!(pegged.peg.literal!("\r"),
                        pegged.peg.literal!("\n"))), pegged.peg.any))), "SrtGrammar.Line")(TParseTree("",
                        false, [], s));
            }
            else
            {
                forgetMemo();
                return hooked!(pegged.peg.defined!(pegged.peg.fuse!(
                        pegged.peg.oneOrMore!(pegged.peg.and!(
                        pegged.peg.negLookahead!(pegged.peg.or!(pegged.peg.literal!("\r"),
                        pegged.peg.literal!("\n"))), pegged.peg.any))), "SrtGrammar.Line"), "Line")(TParseTree("",
                        false, [], s));
            }
        }

        static string Line(GetName g)
        {
            return "SrtGrammar.Line";
        }

        static TParseTree opCall(TParseTree p)
        {
            TParseTree result = decimateTree(Subtitles(p));
            result.children = [result];
            result.name = "SrtGrammar";
            return result;
        }

        static TParseTree opCall(string input)
        {
            if (__ctfe)
            {
                return SrtGrammar(TParseTree(``, false, [], input, 0, 0));
            }
            else
            {
                forgetMemo();
                return SrtGrammar(TParseTree(``, false, [], input, 0, 0));
            }
        }

        static string opCall(GetName g)
        {
            return "SrtGrammar";
        }

        static void forgetMemo()
        {
            memo = null;
        }
    }
}

alias GenericSrtGrammar!(ParseTree).SrtGrammar SrtGrammar;
